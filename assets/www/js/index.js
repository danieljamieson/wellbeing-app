app.initialize();

// Setting up Local storage
var storage = window.localStorage;
var usePassword = storage.getItem("usePassword");
var username = storage.getItem("username");

// This variable is used to hold the total number of records that have been added to memory in the app's history
// In actuality it used more like a pointer rather than a total number of records (where recordCount + 1 is the next free space)
var recordCount = storage.getItem("recordCount");

// Required for returning to the correct page
sessionStorage.setItem("previous_page", "index");

var providedPassword = sessionStorage.getItem("providedPassword");

initWelcome();

initRecords();

initPassword();

var data = {
    personid: "Christophe",
};

/*
 * Lets check for a pin
 * -PIN is retrieved from database when records have been uploaded
 * -Give option to delete PIN
 */
var pin = localStorage.getItem("pin");
//alert(pin);
if(pin != null) {
    $('#removetransfer').show();
} else {
	  $('#transfer').show();
}

//Default values for each page!
data['thinkPageTemplate'] = {
    key_negative_thought: "",
    
    for_thoughts: [
                   {"textarea_id": "0", "link": true, "textarea_value":""}
                   ],
    
    against_thoughts: [
                       {"textarea_id": "0", "link": true, "textarea_value":""}
                       ],
    
    balanced_thought: ""
};

data['eventPageTemplate'] = {
    title_field: "",
    what_happened_field: ""
};

data['feelingPageTemplate'] = {
    emotion_box: [{
                  options:{val:2},
                  emotion_slider_value:10,
                  "link":true
                  }],
    
    feeling_negative_thoughts: [{"feeling_negative_thoughts_id":0, "link":true, "key_thought":false, "feeling_negative_thoughts_value":""}]
};

data['behaviourPageTemplate'] = {
    duringevent: [ {"duringevent_id":"0", "link":true, "duringevent_value":""}],
    afterevent: [ {"afterevent_id":"0", "link":true, "afterevent_value":""}],
    what_happened_field: ""
};


data['symptomMapTemplate'] = {
head_list: [{"val": ""}],
arm_list: [{"val": ""}],
body_list: [{"val": ""}],
shoulder_list: [{"val": ""}],
leg_list: [{"val": ""}]
};

try {
    storage.setItem("default_data", JSON.stringify(data));
    storage.setItem("active_data", JSON.stringify(data));
    $('#currentdata').html(localStorage.getItem("active_data"));
} catch (e) {
    if (e == QUOTA_EXCEEDED_ERR) {
        alert('Quota exceeded!'); //data wasn't successfully saved due to quota exceed so throw an error
    }
}

$('.load').click(function() {                 
    $.post("http://homepages.cs.ncl.ac.uk/patrick.mc-corry/wellbeing/getRecord.php", {pin: 4853}
    ).done(function(server_data_encoded) {
     	  var server_data = JSON.parse(server_data_encoded);
     	
     	  if (server_data['result'] === true) {
     		    alert("success");
     		    localStorage.clear();
     		    localStorage.setItem("recordCount", server_data['data'].length);
     		
     		    for (var i=0; i<server_data['data'].length; i++) {
     			      localStorage.setItem(i, JSON.stringify(server_data['data'][i]));
     		    }
     	  } else {
     		    alert("PIN does not exist");
     	  }
                       
        try {
        	//localStorage.setItem("active_data", JSON.stringify(server_data));
        	//$('#currentdata').html(server_data_encoded);
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
            	alert('Quota exceeded!'); //data wasn't successfully saved due to quota exceed so throw an error
            }
        }
    });
});

var uploading = false;
/*
 * Upload records to the database
 */
$('#transfer').click(function() {
    if (uploading) {
        alert("Giving your therapist access... please wait");
    } else {
        uploading = true;
        var list = new Array();
        var recordCount = localStorage.getItem("recordCount");

        for (var i=1; i<=recordCount; i++) {
            var record_index = new String(i);
            var record = localStorage.getItem(record_index);
            
            if (record != null) {
		            list.push(JSON.parse(record));
            }
        }

        if (list.length >= 1) {            
            $.post("http://homepages.cs.ncl.ac.uk/patrick.mc-corry/wellbeing/setRecord.php", {
                list_data: JSON.stringify(list), person_id: localStorage.getItem("person_id") 
            }).done(function(results) {
 	              results = JSON.parse(results);
 	              $("#Transfer-popup").html("<h1>" + 
                                          results['pin'] + 
                                          "</h1><p>Please give this number to your therapist</p>" +
                                           results['data']
                                        );
	              localStorage.setItem("pin", results['pin']);
                localStorage.setItem("person_id", results['person_id']);
	   
                $('#transfer').hide();
          	    ('#removetransfer').show();
          	    uploading = false;
            });
        } else {
            alert("No records found");
        }
    }
});

/* Activated when user hits the 'RemoveTransfer' button. 
* Makes a deletion request to the database with the pin and id of the user. */
$('#removetransfer').click(function(){
    $.post("http://homepages.cs.ncl.ac.uk/patrick.mc-corry/wellbeing/deleteRecord.php", {
        pin: localStorage.getItem("pin"), 
        person_id: localStorage.getItem("person_id")
    }
    ).done(function(results_json) {
       	var results = JSON.parse(results_json);
       	$("#Transfer-popup").html("<p>Your therapist can no longer access your records</p>");
       	localStorage.removeItem("pin");
       	$('#removetransfer').hide()
        $('#transfer').show();
    });
});

/* Activated when the user submits a password. 
* Hash the password with the stored salt and then compare it with the hash stored locally*/
$('#submit_password').click(function(){
    var entered_pass = $('#enter-password').val();
    var stored_pass = storage.getItem("password");

    if (entered_pass == stored_pass) {
        sessionStorage.setItem("providedPassword", "true");
        var magnificPopup = $.magnificPopup.instance; 
        magnificPopup.close(); 
    }
    else {
        $("#incorrect-pass").text("Incorrect password, try again");
    }
});

function openPasswordPopup() {
    $(".open-Password-popup").trigger('click');
}                                       

//Pop up info section
$('.open-Transfer-link').magnificPopup({
    src: '#Transfer-popup',
    type: 'inline',
    midClick: true,
    removalDelay: 300,
    mainClass: 'mfp-fade',
    showCloseBtn: false,
    closeOnBgClick: false
});

$(".open-Password-popup").magnificPopup({
    src: '#Password-popup',
    type: 'inline',
    midClick: true,
    mainClass: 'mfp-fade',
    modal: true
});

/*
* initWelcome - displays welcome message
*/
function initWelcome() {
    /* Set up welcome message with user's name */
    if (username == undefined) {
      $("#welcome_message").text("Welcome back, John Doe");
    }
    else {
      $("#welcome_message").text("Welcome back, " + username);
    }
}

/*
* initRecords - setups records on first time use
*/
function initRecords() {
    /* If this is our first view of the app, we need to set the recordCount to 0 */
    if (recordCount == undefined) {
      var count = 0;
      try {
          storage.setItem("recordCount", count);
      } catch (e) {
          if (e == QUOTA_EXCEEDED_ERR) {
              alert('Quota exceeded!');
          }
      }
      recordCount = storage.getItem("recordCount");
    }
}

/*
* initRecords - setups the password if first time use and checks password if already configured
*/
function initPassword() {
    /* If this is out first view of the app, we need to set the option to use password to false*/
    if (usePassword == undefined) {
      usePassword = "false";
      //alert("use pin is now " + usePin);
      try {
          storage.setItem("usePassword", usePassword);
      } catch (e) {
          if (e == QUOTA_EXCEEDED_ERR) {
              alert('Memory Quota exceeded!');
          }
      }
    }

    // If the user has a password configured we should ask them for it
    if (usePassword == "true" && providedPassword != "true") {
        openPasswordPopup();
    }
}