//
// Query the database
//

function hiss(db) {
	db.transaction(insertRecord, errorDB, successDB);
    db.transaction(selectRecord, errorDB);
}

function selectRecord(tx) {
	var table = "RECORD";
	var item = "*";
	
    
	tx.executeSql('SELECT * FROM RECORD', [], displayRecords, errorDB); 
}

function insertRecord(tx) {

	var table = "RECORD";
	var titles = new Array("title", "time_created", "before_rating", "after_rating");
	var values = new Array("'first entry'", "'15:00'", "1", "7");
    
    alert('INSERT INTO ' + table + '(' + titles.join() + ') VALUES (' + values.join() + ')');
    tx.executeSql('INSERT INTO ' + table + '(' + titles.join() + ') VALUES (' + values.join() + ')');
	
}

function displayRecords(tx, results) {
    var len = results.rows.length;
    var record_array = new Array();
    
    alert(len);
    for (var i=0; i<len; i++){
        
        var result = {"title": results.rows.item(i).title};
        record_array.push(result);
    }
    
    var result = new Array();
    
    result['list_item'] = record_array;
    
    $.Mustache.load('./templates/recordArchiveTemplate.html').done(function () {
        $('#record_container').html("");
        $('#record_container').mustache('recordListTemplate', result);
    });

    
    
}

//
// Transaction error callback
//
function errorDB(err) {
	console.log("Error processing SQL: "+err.code);
}
//
// Transaction success callback
//
function successDB() {
	console.log("Successful callback");
    //alert("worked");
}
