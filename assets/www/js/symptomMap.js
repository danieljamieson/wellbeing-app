function updateBodyPart(name_of_list, list_from_memory, body_parts) {

	for(var i=0; i<list_from_memory.length; i++) {
		for(var j=0; j<body_parts[name_of_list].length; j++) {
			if(body_parts[name_of_list][j]['val'] == list_from_memory[i]['val']) {
				body_parts[name_of_list][j]['unselected'] = false;
				body_parts[name_of_list][j]['selected'] = true;
                body_parts[name_of_list][j]['slider_data'] = list_from_memory[i]['slider_data'];
				break;
			}
		}
	}
	
}


var data = JSON.parse(localStorage.active_data);

var body_parts = {
	head_list: [{"val": "Headaches", "unselected": true}, {"val": "Dizziness", "unselected":true}, {"val": "Feeling Faint", "unselected":true}],
	arm_list: [{"val": "Pins and needles", "unselected": true}, {"val": "Cold hands", "unselected":true}, {"val": "Shaking hands", "unselected":true}],
	body_list: [{"val": "Pounding Heart", "unselected": true}, 
				{"val": "Shortness of breath", "unselected":true}, 
				{"val": "Over-breathing", "unselected":true},
				{"val": "Feeling of nausea", "unselected": true}, 
				{"val": "Stomach cramps", "unselected":true}, 
				{"val": "'Butterflies'", "unselected":true},
				{"val": "Diarrhoea", "unselected": true}],
	shoulder_list: [{"val": "Neck tension", "unselected": true}, {"val": "Shoulder tension", "unselected":true}],
	leg_list: [{"val": "Shaky Legs", "unselected": true}]
};

updateBodyPart("head_list", data['symptomMapTemplate']['head_list'], body_parts);
updateBodyPart("arm_list", data['symptomMapTemplate']['arm_list'], body_parts);
updateBodyPart("body_list", data['symptomMapTemplate']['body_list'], body_parts);
updateBodyPart("shoulder_list", data['symptomMapTemplate']['shoulder_list'], body_parts);
updateBodyPart("leg_list", data['symptomMapTemplate']['leg_list'], body_parts);

$.Mustache.load('./templates/symptomMapTemplates.html').done(function () {
      $('#displayArea').html("");
      $('#displayArea').mustache('symptomMapTemplate', body_parts);
});

$('.open-Head-link').magnificPopup({
	    src: '#Head-popup',
	    type: 'inline',
	    midClick: true,
	    removalDelay: 300,
	    mainClass: 'mfp-fade'
});

$('.open-Shoulders-link').magnificPopup({
		src: '#Shoulders-popup',
		type: 'inline',
		midClick: true,
		removalDelay: 300,
		mainClass: 'mfp-fade'
});
$('.open-Arms-link').magnificPopup({
	    src: '#Arms-popup',
	    type: 'inline',
	    midClick: true,
	    removalDelay: 300,
	    mainClass: 'mfp-fade'
});
$('.open-Body-link').magnificPopup({
	    src: '#Body-popup',
	    type: 'inline',
	    midClick: true,
	    removalDelay: 300,
	    mainClass: 'mfp-fade'
});
$('.open-Legs-link').magnificPopup({
	    src: '#Legs-popup',
	    type: 'inline',
	    midClick: true,
	    removalDelay: 300,
	    mainClass: 'mfp-fade'
});

$('.open-NewSymptom-link').magnificPopup({
		src: '#NewSymptom-popup',
		type: 'inline',
		midClick: true,
		removalDelay: 300,
		mainClass: 'mfp-fade'
});

$('.save').click(function(){
	var selected_options = "";
	
	selected_options['head_list'] = new Array();
	
	var head = $('#Head-popup').find('.symptomSelected');
	var shoulders = $('#Shoulders-popup').find('.symptomSelected');
	var arms = $('#Arms-popup').find('.symptomSelected');  
	var body = $('#Body-popup').find('.symptomSelected'); 
	var legs = $('#Legs-popup').find('.symptomSelected'); 

	var head_list = new Array();
	jQuery.each( head, function() {
        var slider = $(this).next().find(":input").first().attr("value");
		var result = {"val" : $(this).html(), "slider_data":slider};
		head_list.push(result);
	});
	
	data['symptomMapTemplate']['head_list'] = head_list;
	
	var shoulder_list = new Array();
	jQuery.each( shoulders, function() {
        var slider = $(this).next().find(":input").first().attr("value");
		var result = {"val" : $(this).html(), "slider_data":slider};
		shoulder_list.push(result);
	});
	
	data['symptomMapTemplate']['shoulder_list'] = shoulder_list;
	
	var body_list = new Array();
	jQuery.each( body, function() {
        var slider = $(this).next().find(":input").first().attr("value");
		var result = {"val" : $(this).html(), "slider_data":slider};
		body_list.push(result);
	});
	
	data['symptomMapTemplate']['body_list'] = body_list;
	
	var arm_list = new Array();
	jQuery.each( arms, function() {
        var slider = $(this).next().find(":input").first().attr("value");
		var result = {"val" : $(this).html(), "slider_data":slider};
		arm_list.push(result);
	});
	
	data['symptomMapTemplate']['arm_list'] = arm_list;
	
	var leg_list = new Array();
	jQuery.each( legs, function() {
        var slider = $(this).next().find(":input").first().attr("value");
		var result = {"val" : $(this).html(), "slider_data":slider};
		leg_list.push(result);
	});
	
	data['symptomMapTemplate']['leg_list'] = leg_list;
	localStorage.active_data = JSON.stringify(data);
});