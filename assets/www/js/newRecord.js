function fixFeelingPage(template_no_hash, data) {
    if(template_no_hash == "feelingPageTemplate") {
        var emotions = data[template_no_hash]["emotion_box"];
        
        //alert(JSON.stringify(emotions) + " and the length is " + emotions.length);
        var new_emotions = new Array();
        
        for(var i=0; i<emotions.length; i++) {
            
            //alert(emotions[i]["options"]["val"] + " ... " + emotions[i]["emotion_slider_value"]);
            new_emotions.push(getEmotion(emotions[i]["options"]["val"], emotions[i]["emotion_slider_value"]));
        }
        
        //alert(JSON.stringify(new_emotions));
        
        data[template_no_hash]["emotion_box"] = new_emotions;
        
    }
}

var storage = window.localStorage;
var stored_templates = new Array();
var previous_template_used = "";
var links = ["#eventLink", "#thinkLink", "#behaviourLink", "#feelingLink"];

var data;

// Check to see if we're viewing an old record or creating a new one
var recordToView = storage.getItem("recordToView");
if (recordToView == undefined) {
    // Creating a new record
    var activeData = storage.getItem("active_data");
    if (activeData == undefined) {
        data = JSON.parse(storage.getItem("default_data"));
    } else {
        data = JSON.parse(activeData);
    }
}
else {
    // Load and display an old record
    var record = storage.getItem(recordToView);
    data = JSON.parse(record);
    storage.removeItem("recordToView");
}

storage.setItem("active_data", data);

var potential_hash = window.location.hash;

if(potential_hash.length > 1) {
    previous_template_used = location.hash.slice(1);
} else {
    previous_template_used = "eventPageTemplate";
}

fixFeelingPage(previous_template_used, data);

switch(previous_template_used) {
    case "eventPageTemplate":
        $(links[0]).addClass("selectedTab");
        break;
    case "feelingPageTemplate":
        $(links[3]).addClass("selectedTab");
        break;
    case "behaviourPageTemplate":
        $(links[2]).addClass("selectedTab");
        break;
    case "thinkPageTemplate":
        $(links[1]).addClass("selectedTab");
        break;
}//

$.Mustache.load('./templates/newRecordTemplates.html').done(function () {
  $('#displayArea').html("");
  $('#displayArea').mustache(previous_template_used, data[previous_template_used]);
});

function storeResults(template) {
    
    /*
     * Establish which page should be loaded
     */
    var template_no_hash = template.replace('#', '');
    
    /*
     * Now we want to clear the screen and display
     * the relevant questions for the selected tab
     * (so if behaviour tab is tapped - show behaviour
     *  related questions)
     */
    
    //Lets store the fields filled in by the user
    switch(previous_template_used) {
            
        case "feelingPageTemplate":
            var neg_array = new Array();
            var emotions_array = new Array();
            
            /*
             * Find each 'select box' for emotions
             * Retrieve the option selected by the user 'nervous'
             * Create JSON element using 'setEmotion' - will contain the SINGLE selected option (and not all possible options)
             * Keep a list of each selected item
             */
            $('#emotions .item').each(function () {
                                      
                                      var item = $(this).find("select");
                                      var selectValue = item.val();
                                      
                                      item = $(this).find("input[type=range]");
                                      sliderValue = item.val();
                                      
                                      var result = setEmotion(selectValue, sliderValue);
                                      
                                      emotions_array.push(result);
                                      });
            
            //Go through each "during event" textarea and get the evntry for each text field
            $('#negative_thoughts textarea').each(function () {
                                                  
                                                  var value = this.value.trim();
                                                  var key_thought = $(this).data('key_thought');
                                                  
                                                  if(key_thought==undefined) {
                                                  key_thought = false;
                                                  }
                                                  
                                                  if(value != "") {
                                                  var result = {"feeling_negative_thoughts_id": 0, "link": true, "key_thought":key_thought, "feeling_negative_thoughts_value":value};
                                                  
                                                  neg_array.push(result);
                                                  }
                                                  
                                                  //alert(this.value); // "this" is the current element in the loop
                                                  });
            
            if(neg_array.length < 1) {
                neg_array = [{"feeling_negative_thoughts_id":0, "link":true, "key_thought":0, "feeling_negative_thoughts_value":""}];
            }
            
            if(emotions_array.length < 1) {
                emotions_array = [setEmotion(1, 10)];
            }
            
            data[previous_template_used]["emotion_box"] = emotions_array;
            data[previous_template_used]["feeling_negative_thoughts"] = neg_array;
            
            break;
            
        case "behaviourPageTemplate":
            var during_array = new Array();
            var after_array = new Array();
            
            //Go through each "during event" textarea and get the entry for each text field
            $('#behaviours-during textarea').each(function () {
                                                  
                                                  var value = this.value.trim();
                                                  
                                                  if(value != "") {
                                                  var result = {"duringevent_id": during_array.length, "link": true, "duringevent_value":this.value};
                                                  
                                                  during_array.push(result);
                                                  }
                                                  });
            
            //Go through each "during event" textarea and get the entry for each text field
            $('#behaviours-after textarea').each(function () {
                                                 
                                                 var value = this.value.trim();
                                                 
                                                 if(value != "") {
                                                 var result = {"afterevent_id": during_array.length, "link": true, "afterevent_value":this.value};
                                                 
                                                 after_array.push(result);
                                                 }
                                                 });
            
            if(during_array.length < 1) {
                during_array = {"duringevent_id": 0, "link": true, "duringevent_value":""};
            }
            
            if(after_array.length < 1) {
                after_array = {"afterevent_id": 0, "link": true, "afterevent_value":""};
            }
            
            data[previous_template_used]["duringevent"] = during_array;
            data[previous_template_used]["afterevent"] = after_array;
            
            break;
        case "thinkPageTemplate":
            var for_array = new Array();
            var against_array = new Array();
            
            //Go through each "for thoughts" textarea and get the entry for each text field
            $('#container_for textarea').each(function () {
                                              
                                              var value = this.value.trim();
                                              
                                              if(value != "") {
                                              var result = {"textarea_id": for_array.length, "link": true, "textarea_value":this.value};
                                              
                                              for_array.push(result);
                                              }
                                              //alert(this.value); // "this" is the current element in the loop
                                              });
            
            //Go through each "against thoughts" textarea and get the entry for each text field
            $('#container_against textarea').each(function () {
                                                  var value = this.value.trim();
                                                  
                                                  if(value != "") {
                                                  var result = {"textarea_id": against_array.length, "link": true, "textarea_value":this.value};
                                                  against_array.push(result);
                                                  }
                                                  //alert(this.value); // "this" is the current element in the loop
                                                  });
            
            
            var value = $('#balanced_thought').val().trim();
            
            if(for_array.length < 1) {
                for_array = {"textarea_id": 0, "link": true, "textarea_value":""};
            }
            
            if(against_array.length < 1) {
                against_array = {"textarea_id": 0, "link": true, "textarea_value":""};
            }
            
            data[previous_template_used]["for_thoughts"] = for_array;
            data[previous_template_used]["against_thoughts"] = against_array;
            data[previous_template_used]["balanced_thought"] = value;
            
            break;
            
        case "eventPageTemplate":
            var title = $('#title_field').val().trim();
            var what_happened = $('#what_happened_field').val().trim();
            
            data[previous_template_used]['title_field'] = title;
            data[previous_template_used]['what_happened_field'] = what_happened;
            //alert(title);
            
            break;
            
        default:
            break;
    }
    
    /*
     * Make sure a key negative thought is available
     * before opening the thinkPageTemplate
     */
    if(template_no_hash == "thinkPageTemplate") {
        var found_key_thought = false;
        
        for(var i=0; i<data["feelingPageTemplate"]["feeling_negative_thoughts"].length; i++) {
            //alert(data["feelingPageTemplate"]["feeling_negative_thoughts"][i]["key_thought"]);
            if(data["feelingPageTemplate"]["feeling_negative_thoughts"][i]["key_thought"] == 1) {
                found_key_thought = true;
                data["thinkPageTemplate"]["key_negative_thought"] = data["feelingPageTemplate"]["feeling_negative_thoughts"][i]["feeling_negative_thoughts_value"];
                break;
            }
        }
        
        if(!found_key_thought) {
            if(data["feelingPageTemplate"]["feeling_negative_thoughts"][0]["feeling_negative_thoughts_value"] == "") {
                alert("You cannot evaluate your thoughts yet. Please write how you feel first");
                return false;
            } else {
                //alert("new value: " + data["feelingPageTemplate"]["feeling_negative_thoughts"][0]["feeling_negative_thoughts_value"]);
                data["thinkPageTemplate"]["key_negative_thought"] = data["feelingPageTemplate"]["feeling_negative_thoughts"][0]["feeling_negative_thoughts_value"];
                data["feelingPageTemplate"]["feeling_negative_thoughts"][0]["key_thought"] = 1;
            }
        }
        
    }
    
    fixFeelingPage(template_no_hash, data);
    
    /*
     * Tapping a tab (such as behaviour) will activate
     * this function. We want to make sure all tabs have the
     * same background (none look selected) and update the tapped
     * tabs background - to show it has been selected.
     */
    for(var i=0; i<links.length; i++) {
        $(links[i]).removeClass("selectedTab");
    }
    
    switch(template_no_hash) {
        case "eventPageTemplate":
            $(links[0]).addClass("selectedTab");
            break;
        case "feelingPageTemplate":
            $(links[3]).addClass("selectedTab");
            break;
        case "behaviourPageTemplate":
            $(links[2]).addClass("selectedTab");
            break;
        case "thinkPageTemplate":
            $(links[1]).addClass("selectedTab");
            break;
    }
    
    
    //stored_templates[previous_template_used] = $('#displayArea').html();
    previous_template_used = template_no_hash;
    
    //alert("not found template" + template + "-" + previous_template_used);
    $('#displayArea').html("");
    
    $('#displayArea').mustache(template_no_hash, data[template_no_hash]);
    
}

function setEmotion(selected, slider) {
    
    var options = {"options": {val:selected},
        "emotion_slider_value": slider,
        "link":true};
    
    return options;
}

function getEmotion(selected, slider) {
    
    var options = {"options": [{val:1, txt:'Anxious', sel:false},
                               {val:2, txt:'Nervous', sel:false},
                               {val:3, txt:'Scared', sel:false},
                               {val:4, txt:'Angry', sel:false},
                               {val:5, txt:'Frustrated', sel:false},
                               {val:6, txt:'Overwhelmed', sel:false},
                               {val:7, txt:'Sad', sel:false},
                               {val:8, txt:'Depressed', sel:false},
                               {val:9, txt:'Upset', sel:false},
                               {val:10, txt:'Irritable', sel:false},
                               {val:11, txt:'Stressed', sel:false},
                               {val:12, txt:'Hopeless', sel:false},],
        "emotion_slider_value": slider,
        "link":true};
    
    for(var i=0; i<options['options'].length; i++) {
        
        if(options['options'][i]['val'] == selected) {
            options['options'][i]['sel'] = true;
        }
    }
    
    return options;
}

function goBack() {
    
    var previous_page = sessionStorage.getItem("previous_page");
    if(previous_page == "allrecords") {
        window.location = "RecordArchive.html";
        
    } else {
        window.location = "index.html";
    }
}

$('.cancel').click(function(){
    goBack();
});

$(function(){
    /* Activated when a user hits save */
    $('.save').click(function() {
        var today = new Date();
        data['eventPageTemplate']['time_created'] = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
        
        storeResults(previous_template_used);
        var recordCount = storage.getItem("recordCount");
        var record = JSON.stringify(data);
   
        if (recordToView != undefined) {
            // If we're viewing an old record, make sure to save it to the old record's position
            try {
                storage.setItem(recordToView, record);
            } catch (e) {
                if (e == QUOTA_EXCEEDED_ERR) {
                    alert('Memory Quota exceeded!');
                }
            }
        } else {
            // If we're starting a new record, use the next free position to save it
            var index = parseInt(recordCount) + 1;
            var indexStr = index + "";

            try {
                storage.setItem(indexStr, record);
                storage.setItem("recordCount", index);
            } catch (e) {
                if (e == QUOTA_EXCEEDED_ERR) {
                    alert('Memory Quota exceeded!');
                }
            }
        }
        storage.removeItem("active_data");
        goBack();
    });
    $('.openPage').click(function() {storeResults($(this).attr('href'));});  
});

var new_field= "<div class=\"item\"><textarea class=\"topcoat-text-input\"></textarea><a href=\"#remove\"><img class=\"icon-remove\" src=\"img/glyphicons_207_remove_2.png\" /></a></div>";

var new_emotion = "<div class=\"item\"><p class=\"emotion-input\"><select id = \"select_emotions\" style=\"width:80%;\"><option value = \"1\">Anxious</option><option value = \"2\">Nervous</option><option value = \"3\">Scared</option><option value = \"4\">Angry</option><option value = \"5\">Frustrated</option><option value = \"6\">Overwhelmed</option><option value = \"7\">Sad</option><option value = \"8\">Depressed</option><option value = \"9\">Upset</option><option value = \"10\">Irritable</option><option value = \"11\">Stressed</option><option value = \"12\">Hopeless</option><option value = \"13\">Custom</option></select></p><p class=\"emotion-slider\"><input type=\"range\" min=\"0\" max=\"10\" step=\"1\" value=\"0\"><a href=\"#remove\"><img class=\"emotion-icon-remove\" src=\"img/glyphicons_207_remove_2.png\" style=\"remove-img\" /></a></p></div>";

var new_emotion_textbox = "<textarea class=\"topcoat-text-input\" style=\"width:100%;\"></textarea>";


$('a[href="#remove"]').live('click',function(e) {
    $(this).closest('div').remove();
});