/*
* RecordArchive.js
* This file handles the functionality of viewing and deleting records in RecordArchive.html
*/

var storage = window.localStorage;
var container = document.getElementById("recordContainer"); // the div containing the list of records

// These variables are used when adding a new record to display 
var elementHead = "class=\"openRecord\" href=\"NewRecord.html\"><li class=\"topcoat-list__item\">";
var elementTail = "</li></a>";
var removeHead = "<a href=\"#remove\" id=\"";
var removeTail = "\"><img class=\"archive-icon-remove\" src=\"img/glyphicons_207_remove_2.png\" /></a>";

// There are two counters
// The first is stored in memory and acts as a pointer to the last used index (therefore the next free space is +1 from this)
// The second is the number of non-null records that were successfully read from memory 
var recordCount = storage.getItem("recordCount");
var actualRecordCounter = 0;

if (recordCount > 0) { // Skip loading records if there are non to display
	for (var i = 0; i <= recordCount; i++) {
		var indexStr = i + "";
		var record = storage.getItem(indexStr); // retrieve the record

		if (record != null && record != undefined) {
		  // Construct a list item using the parsed JSON and add it to the container
		  var data = JSON.parse(record);
		  var title = data['eventPageTemplate']['title_field'];
		  var time = data['eventPageTemplate']['time_created'];
		  var details = data['eventPageTemplate']['what_happened_field'];

		  var newElement = "<a id=\"" + i + "\" " + elementHead + "<h2>" + title + "</h2> <h3>" + time + removeHead + i + removeTail + "</h3> <p>" + details + "</p>"
		  container.innerHTML = container.innerHTML + newElement;
		  actualRecordCounter++;
		}
	}
}

// If this counter is still 0, then there are no records being displayed, so we should display a message instead
if (actualRecordCounter == 0) {
	container.innerHTML = "<li class=\"topcoat-list__item\"><h2>There are currently no records in the archive</h2></li>";
}

/* Activated when a user clicks on a record */
$('.openRecord').click(function() {
  	sessionStorage.setItem("previous_page", "allrecords");
	var index = $(this).attr('id'); // retrieve the ID of the record
 	var indexStr = index + "";

 	// Opening a record is done by setting the 'recordToView' var to the index of the record in memory
 	// When navigation to the NewRecord page is complete, the non-null 'recordToView' causes that record to be loaded
  	try {
		window.localStorage.setItem("recordToView", indexStr);
	} catch (e) {
		if (e == QUOTA_EXCEEDED_ERR) {
			alert('Quota exceeded!');
		}
	}
});

/* Activated when the user clicks on the remove icon, displayed on the date next to the record*/
$('a[href="#remove"]').live('click',function(e) {
	var index = $(this).attr('id')
	window.localStorage.removeItem(index); // removes the item from memory
	$(this).closest('li').remove(); // removes the item from the list
	
	actualRecordCounter--;
	if (actualRecordCounter == 0) {
	  container.innerHTML = "<li class=\"topcoat-list__item\"><h2>There are currently no records in the archive</h2></li>";
	}
});
