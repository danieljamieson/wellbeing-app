<?php
    
    require("login_admin.php");
    
    function addList($aList, $id_slot, $slot, $question) {
        
        foreach($aList as $item) {
            $response = $item[$slot];
            
            $response = trim($response);
            
            echo "........" . sizeof($response) . "and it is... " . $response;
            if(strlen($response) < 2) {
                $response="empty";
            }
            
            $answer = R::dispense('answer');
            $answer->data = $response;
            R::store($answer);
            
            $questionanswer = R::dispense('questionanswer');
            $questionanswer->question = $question;
            $questionanswer->answer = $answer;
            R::store($questionanswer);
        }
    }
    
    R::nuke();
    
    
    //print_r(json_decode($_POST['data']));
    $mydata = json_decode($_POST['data'], true);
    
    //echo "Printing mydata....";
    //print_r($mydata);
    
    //Get a record from the database
    $record = "";
    
    if(isset($mydata['record_id'])) {
        $record = R::load('record', $mydata['record_id']);
        
    } else {
        $record = R::dispense('record');
        $record->time_created = date('Y-m-d H:i:s');
        $record->title = $mydata['eventPageTemplate']['title_field'];
        $record->description = $mydata['eventPageTemplate']['what_happened_field'];
        R::store($record);
    }
    
    //-------------------
    // Store 'after event' answers
    //-------------------
    
    $question = R::dispense('question');
    $question->description = "Emotions felt by the person and the intensity of their feelings";
    R::store($question);
    
    $emotions = $mydata['feelingPageTemplate']['emotion_box'];
    
    foreach($emotions as $emo) {
        $options = $emo['options'];
        
        $answer = R::dispense('answer');
        
        for($i=0; $i<sizeof($options); $i++) {
            echo $options[$i]['val'] . ' ... ' . $options[$i]['txt'] . ' ... ' . $options[$i]['sel'] . ' <br>';
            
            if($options[$i]['sel'] === true) {
                $answer->data = strval($options[$i]['val']);
                break;
            }
        }
        
        $answer->slider_data = $emo['emotion_slider_value'];
        
        R::store($answer);
        
        $questionanswer = R::dispense('questionanswer');
        $questionanswer->question = $question;
        $questionanswer->answer = $answer;
        R::store($questionanswer);
    }
    
    
    //-------------------
    // Store 'negative thoughts' answers
    //-------------------
    $question = R::dispense('question');
    $question->description = "Negative thoughts";
    R::store($question);
    
    $negative_thought = $mydata['feelingPageTemplate']['feeling_negative_thoughts'];
    
    print_r($negative_thought);
    
    foreach($negative_thought as $neg) {
        //print_r($neg);
        $response = $neg['feeling_negative_thoughts_value'];
        
        $response = trim($response);
        
        //echo "........" . sizeof($response);
        if(strlen($response) < 2) {
            $response="empty";
        }
        
        $answer = R::dispense('answer');
        $answer->data = $response;
        R::store($answer);
        
        $questionanswer = R::dispense('questionanswer');
        $questionanswer->question = $question;
        $questionanswer->answer = $answer;
        R::store($questionanswer);
    }
    
    //-------------------
    // Store 'during event' answers
    //-------------------
    
    $duringevent = $mydata['behaviourPageTemplate']['duringevent'];
    $question = R::dispense('question');
    $question->description = "During event";
    R::store($question);
    
    addList($duringevent, 'duringevent_id', 'duringevent_value', $question);
    //-------------------
    // Store 'after event' answers
    //-------------------
    $question = R::dispense('question');
    $question->description = "After event";
    R::store($question);
    
    $afterevent = $mydata['behaviourPageTemplate']['afterevent'];
    
    addList($afterevent, 'afterevent_id', 'afterevent_value', $question);
    
    //-------------------
    // Store 'For event' answers
    //-------------------
    
    $question = R::dispense('question');
    $question->description = "For event";
    R::store($question);
    
    $for_negative_thought = $mydata['thinkPageTemplate']['for_thoughts'];
    
    addList($for_negative_thought, 'textarea_id', 'textarea_value', $question);
    
    //-------------------
    // Store 'Against event' answers
    //-------------------
    
    $question = R::dispense('question');
    $question->description = "Against event";
    R::store($question);
    
    $against_negative_thought = $mydata['thinkPageTemplate']['against_thoughts'];
    
    addList($against_negative_thought, 'textarea_id', 'textarea_value', $question);
    
    $question = R::dispense('question');
    $question->description = "Balanced Thought";
    R::store($question);
    
    $balanced_thought = $mydata['thinkPageTemplate']['balanced_thought'];
    
    if(strlen($balanced_thought) < 2) {
        $balanced_thought = "empty";
    }
    
    $answer = R::dispense('answer');
    $answer->data = $balanced_thought;
    R::store($answer);
    
    $questionanswer = R::dispense('questionanswer');
    $questionanswer->question = $question;
    $questionanswer->answer = $answer;
    R::store($questionanswer);
    
?>